# rusage

Haskell bindings to the linux function
[getrusage](https://linux.die.net/man/2/getrusage). This code is
essentially copied from
[hs-gauge](https://github.com/vincenthz/hs-gauge/tree/master).
